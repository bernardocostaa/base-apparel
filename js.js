const btn = document.getElementById('btn')
const areaInput = document.querySelector('.div-input')
const inputBorda = document.querySelector(".div-input");

function checkEmail(event){
    event.preventDefault()
    const inputEmail = document.getElementById("inputEmail");
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const inputValue = inputEmail.value.trim();
    const valideInput = emailRegex.test(inputEmail.value);
    if(inputValue && valideInput ){
        removeErro()
    }else{
        showErro('Please provide a valid email')
    }
}

function showErro(msg){
    if(!document.querySelector('.errorP')){
        let msgEl = document.createElement('p')
        let imgErro = document.createElement('img')
        imgErro.setAttribute("src", "./img/icon-error.svg")
        imgErro.classList.add('imgErro')
        inputBorda.classList.add('bordaErr')
        msgEl.innerText = msg
        msgEl.classList.add('errorP')
        areaInput.appendChild(imgErro)
        areaInput.insertAdjacentElement('afterend',msgEl)
    }
}

function removeErro(){
    const error = document.querySelector(".bordaErr");
    let erroImg = document.querySelector(".imgErro");
    let erroP = document.querySelector(".errorP");
    console.log(error,erroImg,erroP);
    if(error){
        inputBorda.classList.remove('bordaErr')
        erroImg.remove()
        erroP.remove()
    }
}

btn.addEventListener('click',checkEmail)